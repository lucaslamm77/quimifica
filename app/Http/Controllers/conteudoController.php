<?php

namespace App\Http\Controllers;

use App\conteudo;
use Illuminate\Http\Request;

class conteudoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\conteudo  $conteudo
     * @return \Illuminate\Http\Response
     */
    public function show(conteudo $conteudo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\conteudo  $conteudo
     * @return \Illuminate\Http\Response
     */
    public function edit(conteudo $conteudo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\conteudo  $conteudo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, conteudo $conteudo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\conteudo  $conteudo
     * @return \Illuminate\Http\Response
     */
    public function destroy(conteudo $conteudo)
    {
        //
    }
}
